# ==APLICACION BASE JSF PRIMEFACES OPEN-SHIFT==
ESTE PROYECTO ES LA BASE PARA SUBIR A OPEN-SHIFT UNA APLICACION FUNCIONAL:

* MAVEN
* JAVA JSF
* PRIMEFACES

El proyecto se creó tomando como base el proyecto kitchensink de open-shift.

* *rhc app create kitchensink jbossas-7 --from-code git://github.com/openshift/kitchensink-example.git*

Basicamente se creo un nuevo proyecto basado en kitchensink y se agregaron las dependencias de primefaces (modificando alguos detalles en el pom.xml)

# El proyecto se puede ejecutar localmente en Netbeans 7.4, en mi caso con la siguiente configuración:

* Netbeans 7.4
* ubuntu 12.04
* servidor web: Apache tomcat 7
* java EE: Java EE 6 Web
* *la variable JAVA_HOME debe estar definida en el sistema, en mi caso es: $JAVA_HOME=/usr/java/jdk1.7.0_07/*


# CONFIGURACION Y DESPLIEGUE

Crear aplicacion en open-shift y tomar el repositorio git para luego subir la aplicacion base
```
$ rhc app create pftest jbossas-7 
```
Clonar la aplicacion base
```
git clone git@bitbucket.org:solaechea/open-shift-jsf-primefaces.git
```

A la aplicación recien clonada agregar el repositorio de la aplicacion creada y subir codigo

```
git remote add osrepo --repositorio aplicacion creada en open shift--
git push -f osrepo master
```
